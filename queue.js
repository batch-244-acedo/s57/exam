let collection = [];

// function to print the entire queue
function print() {
	return collection;
};

// function to add an element to the queue
function enqueue(element) {
    collection[collection.length] = element;
	return collection;    
};

// function to remove the first element from the queue
function dequeue() {
	let newCollection = [];
	for (let i = 1; i < collection.length; i++) {
		newCollection[i-1] = collection[i];
	}
	collection = newCollection
	return collection;
};

// function to get the first element in the queue
function front(){
	return collection[0];
};

// function to get the number of elements in the queue
function size(){
	return collection.length;
};

// function to check if the queue is empty
function isEmpty(){
	return collection.length == 0;
};

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};
